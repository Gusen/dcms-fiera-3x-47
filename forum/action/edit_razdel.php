<?php

$razdel = mysql_fetch_object(mysql_query('SELECT * FROM `forum_razdels` WHERE `id` = '.abs(intval($_GET['razdel']))));

if (user_access('forum_razd_edit') && $razdel) {
    if (isset($_POST['edit'])) {
        $name = mysql_real_escape_string(trim($_POST['name']));
        $description = mysql_real_escape_string(trim($_POST['description']));
        $number = abs(intval($_POST['number']));
        $forum_id = abs(intval($_POST['id_forum']));
        $type = (isset($_POST['type'])) ? 1 : 0;
        $output = (isset($_POST['output'])) ? 1 : 0;
        $isset_name = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_razdels` WHERE `name` = "'.$name.'" AND `id_forum` = '.$forum->id.' AND `id` != '.$razdel->id), 0);
        $razdel_n = ($razdel->number != $number) ? mysql_fetch_object(mysql_query('SELECT `id`, `number` FROM `forum_razdels` WHERE `id_forum` = '.$forum->id.' AND `number` = '.$number.' AND `id` <> '.$razdel->id)) : false;
        if ($razdel_n) {
            mysql_query('UPDATE `forum_razdels` SET `number` = '.$number.' WHERE `id` = '.$razdel->id);
            mysql_query('UPDATE `forum_razdels` SET `number` = '.$razdel->number.' WHERE `id` = '.$razdel_n->id);
        }
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название раздела&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?= $m[0]?></span> символы!.
            </div>
            <?            
        } else           
        if (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>В поле &laquo;Название раздела&raquo; можно использовать от <?= $set['forum_new_them_name_min_pod'] ?> до <?= $set['forum_new_them_name_max_pod'] ?> символов.</div>
            <?
        } elseif ($number < 0 || $number == NULL || $number == 0) {
            ?>
            <div class = 'err'>Введите уровень.</div>
            <?
        } elseif (mb_strlen($description) > 100) {
            ?>
            <div class = 'err'>Слишком длинное описание раздела.</div>
            <?
        } else {
            if ($razdel->id_forum != $forum_id) {
                admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" перенесён из подфорума "'.$forum->name.'" в "'.$new_forum->name.'".');
            }
            if ($razdel->name != $name) {
                admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" переименован в "'.$name.'" в подфоруме "'.$forum->name.'".');
            } else {
                admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" был изменён.');
            }
            mysql_query('UPDATE `forum_razdels` SET `id_forum` = '.$forum_id.', `name` = "'.$name.'", `description` = "'.$description.'", `number` = '.$number.', `type` = '.$type.', `output` = '.$output.' WHERE `id` = '.$razdel->id);
            $_SESSION['msg'] = '<div class = "msg">Раздел успешно изменён.</div>';
            header('Location: '.FORUM.'/'.$forum_id.'/');
            exit;
        }
    } elseif (isset($_POST['delete'])) {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name) ?></a> / Удаление
        </div>
        <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/edit_razdel.html' method = 'post' class="p_m" style = 'text-align: center'>
            Вы уверены, что хотите удалить этот раздел со всем его содержимым?<br />
            <input type = 'submit' name = 'del' value = 'Удалить' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
        </form>
        <?
        include_once '../sys/inc/tfoot.php';
    } elseif (isset($_POST['del'])) {
        admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" был удалён из подфорума "'.$forum->name.'".');
        $themes = mysql_query('SELECT `id` FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id);
        while ($theme = mysql_fetch_object($themes)) {
            $files = mysql_query('SELECT `name` FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            while ($file = mysql_fetch_object($files)) {
                unlink(H.FORUM.'/files/'.$file->name);
            }
            mysql_query('DELETE FROM `forum_post_rating` WHERE `id_theme` = '.$theme->id);
            mysql_query('DELETE FROM `forum_posts` WHERE `id_theme` = '.$theme->id);
            mysql_query('DELETE FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            mysql_query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
            $votes = mysql_query('SELECT `id` FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
            while ($vote = mysql_fetch_object($votes)) {
                mysql_query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
            }
            mysql_query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        }
        mysql_query('DELETE FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id);
        $n_razdels = mysql_query('SELECT `id`, `number` FROM `forum_razdels` WHERE `number` > '.$razdel->number);
        while ($n_razdel = mysql_fetch_object($n_razdels)) {
            mysql_query('UPDATE `forum_razdels` SET `number` = '.($n_razdel->number-1).' WHERE `id` = '.$n_razdel->id);
        }
        mysql_query('DELETE FROM `forum_razdels` WHERE `id` = '.$razdel->id);
        $_SESSION['msg'] = '<div class = "msg">Раздел со всем его содержимым успешно удалён.</div>';
        header('Location: '.FORUM.'/'.$forum->id.'/');
        exit;
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/edit_razdel.html');
        exit;
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name) ?></a> / Редактирование
    </div>
    <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/edit_razdel.html' method = 'post' class="p_m">
        <b>Название раздела (<?= $set['forum_new_them_name_max_pod'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '<?= $razdel->name ?>' /><br /><br />
        <b>Описание раздела (<?= $set['forum_new_them_name_max_pod_opis'] ?> символ(а/ов)):</b><br />
        <textarea name = 'description'><?= $razdel->description ?></textarea><br /><br />
        <b>Подфорум:</b><br />
        <select name = 'id_forum'>
            <?
            $forums_s = mysql_query('SELECT `id`, `name`, `access` FROM `forum` ORDER BY `number` ASC');
            while ($forum_s = mysql_fetch_object($forums_s)) {
                if ($forum_s->access == 0 || ($forum_s->access == 1 && $user['group_access'] > 7) || ($forum_s->access == 2 && $user['group_access'] > 2)) {
                    ?>
                    <option value = '<?= $forum_s->id ?>' <?= ($forum_s->id == $razdel->id_forum) ? 'selected = "selected"' : NULL ?>><?= $forum_s->name ?></option>
                    <?
                } else {
                    continue;
                }
            }
            ?>
        </select><br /><br />
        <b>Позиция:</b> <input type = 'text' name = 'number' value = '<?= $razdel->number ?>' size = '3' /><br />
        <br /><b>Могут создавать темы только:</b><br />
        <label><input type = 'checkbox' name = 'type' value = '1' <?= ($razdel->type == 1) ? 'checked = "checked"' : NULL ?> /> Администраторы + модераторы</label><br />
        <br /><b>Вывод раздела:</b><br />
        <label><input type = 'checkbox' name = 'output' value = '1' <?= ($razdel->output == 1) ? 'checked = "checked"' : NULL ?> /> Отображать посл. 3 темы вместо описания</label><br /><br />
        <input type = 'submit' name = 'edit' value = 'Сохранить' />
        <?= user_access('forum_for_delete') ? "<input type = 'submit' name = 'delete' value = 'Удалить' />" : NULL ?>
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM.'/'.$forum->id ?>/'>Отменить редактирование</a></div>
    <?
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM.'/'.$forum->id.'/');
}
exit;

?>